# Import potřebných knihoven
import matplotlib.pyplot as plt # Vykteslování grafů
import numpy as np
import IPython
from scipy.stats import pearsonr
from scipy.signal import spectrogram, lfilter, freqz, tf2zpk
from scipy.io import wavfile # Pro nahrávání wav souborů

q1_threshold = 0.7
q2_threshold = 0.7

# Vykreslení spektogramu, bod 3

# nahrání souboru
fs, data = wavfile.read('../sentences/sx161.wav')
# normalizace
data = data / 2**15

# ustřednění
data = data - data.mean()

# Hamingovo okno useknutí 0,025 s, 1 vzorek-> 0,0000625 s, 25ms -> 400, 15ms -> 240
freq, time, sgr = spectrogram(data, fs, window=('hamming'), nperseg=400, noverlap=240, nfft=511)
#sgr_log = 10 * np.log10(abs(sgr+1e-20)**2) Spektogram to asi dělá sám ¯\_(ツ)_/¯
sgr_log = 10 * np.log10(sgr+1e-20)

plt.figure(figsize=(9,3))
plt.pcolormesh(time,freq,sgr_log)
plt.gca().set_title('Věta sx161.wav')
plt.gca().set_xlabel('Čas [s]')
plt.gca().set_ylabel('Frekvence [Hz]')
cbar = plt.colorbar()
cbar.set_label('Spektralní hustota výkonu [dB]', rotation=270, labelpad=15)

plt.tight_layout()
#plt.savefig('spectogram_sx161.png')
plt.show()

# Výpočet parametrů (features), bod 4
# Matice features F = A * sgr_log
# výsledek 16* počet rámců
# matice A bude sečítat 16 řádků z matice sgr_log která má 256 řádků
# takže matice A 16x256
a =[[0 for x in range(256)] for y in range(16)]
for i in range(16):
    for j in range(8):
        a[i][i*8+j] = 1

features_sentence = np.dot(a,sgr_log)

# výpočet features pro slovo
fs, data = wavfile.read('../queries/q1.wav')
data = data / 2**15
data = data - data.mean()
freq, time, sgr = spectrogram(data, fs, window=('hamming'), nperseg=400, noverlap=240, nfft=511)
sgr_log = 10 * np.log10(sgr+1e-20)

features_query = np.dot(a,sgr_log)

# Výpočet skóre klíčového slova (query) pro větu
# Transponování matic, aby šly dát vektory sloupců do Pearsonovy korelační funkce
features_sentence_transpose = features_sentence.transpose()
features_query_transpose = features_query.transpose()

# Vektor na výsledky, +1 kvůly poslední korelaci
d = [0 for x in range(np.size(features_sentence, 1) - np.size(features_query, 1) + 1)]

# Zmenšení projíždení abych maticí query nevyjel mimo matici věty
for i in range(np.size(features_sentence, 1) - np.size(features_query, 1) + 1):
    for j in range(np.size(features_query, 1)):
        corelation, tmp = pearsonr(features_query_transpose[j], features_sentence_transpose[i+j])
        d[i] = d[i] + corelation
    
# Podělení hodnot tak aby byly v intervalu 0-1, takže děleno počtem rámců ve query
for i in range(np.size(features_sentence, 1) - np.size(features_query, 1) + 1):
    d[i] = d[i] / np.size(features_query, 1)


################ Obrázky pravděpodobnosty výskytu queries ve větách, bod 6 ##########################
## Nachystání features pro obě query
a =[[0 for x in range(256)] for y in range(16)]
for i in range(16):
    for j in range(8):
        a[i][i*8+j] = 1


# výpočet features q1
fs, data = wavfile.read('../queries/q1.wav')
data = data / 2**15
data = data - data.mean()
freq, time, sgr = spectrogram(data, fs, window=('hamming'), nperseg=400, noverlap=240, nfft=511)
sgr_log = 10 * np.log10(sgr+1e-20)

features_query_1 = np.dot(a,sgr_log)
features_query_1_transpose = features_query_1.transpose()

# výpočet features q2
fs, data = wavfile.read('../queries/q2.wav')
data = data / 2**15
data = data - data.mean()
freq, time, sgr = spectrogram(data, fs, window=('hamming'), nperseg=400, noverlap=240, nfft=511)
sgr_log = 10 * np.log10(sgr+1e-20)

features_query_2 = np.dot(a,sgr_log)
features_query_2_transpose = features_query_2.transpose()

## Vykreslení grafů pro všechny věty
sentences = ['sa1', 'sa2', 'si1601', 'si2231', 'si971', 'sx161', 'sx251', 'sx341', 'sx431', 'sx71']
for sentence in sentences:
    # Vykreslení signálu věty
    fs, data = wavfile.read('../sentences/'+ sentence +'.wav')
    data_to_proces = data / 2**15
    data_to_proces = data_to_proces - data_to_proces.mean()
    t = np.arange(data_to_proces.size) / fs

    figure, image = plt.subplots(3,1)
    image[0].set_title('Výskyt queries pro větu '+ sentence +'.wav')
    image[0].plot(t,data_to_proces)
    image[0].set_xlim(xmin=0)
    image[0].set_xlim(xmax=data_to_proces.size/fs - 0.03) # Když něco odečtu tak se mi osy vyrovnají ¯\_(ツ)_/¯
    image[0].set_ylabel('Signál')
    image[0].set_xlabel('Čas [s]')

    # Vykreslení a výpočet spektogramu features věty
    freq, time, sgr = spectrogram(data_to_proces, fs, window=('hamming'), nperseg=400, noverlap=240, nfft=511)
    sgr_log = 10 * np.log10(sgr+1e-20)

    features_sentence = np.dot(a,sgr_log)

    image[1].pcolormesh(np.arange(np.size(features_sentence, 1))/100, range(16),features_sentence)
    image[1].invert_yaxis()
    image[1].set_ylabel('Features')
    image[1].set_xlabel('Čas [s]')

    # Vykreslení a výpočet skóre že věta obsahuje nějaké query
    features_sentence_transpose = features_sentence.transpose()

    d1 = [0 for x in range(np.size(features_sentence, 1) - np.size(features_query_1, 1) + 1)]
    d2 = [0 for x in range(np.size(features_sentence, 1) - np.size(features_query_2, 1) + 1)]

    for i in range(np.size(features_sentence, 1) - np.size(features_query_1, 1) + 1):
        for j in range(np.size(features_query_1, 1)):
            corelation, tmp = pearsonr(features_query_1_transpose[j], features_sentence_transpose[i+j])
            d1[i] = d1[i] + corelation

    for i in range(np.size(features_sentence, 1) - np.size(features_query_1, 1) + 1):
        d1[i] = d1[i] / np.size(features_query_1, 1)
     
    # Vypsání jestli jsme query ve větě našly a extrakce do souboru
    # Výpočet vzorku: číslo rámce / počet rámců za 1s * vzorkovací frekvence
    # přeskočení pár následujících hodnot kde bude výsledek podobný
    for i in range(np.size(features_sentence, 1) - np.size(features_query_1, 1) + 1):
        if d1[i] >= q1_threshold:
            print('q1 se nachází ve větě '+ sentence +' od vzorku: '+ str(int(i/100*fs)) +' do vzorku: '+ str(int((i+np.size(features_query_1, 1))/100*fs)))
            # extrakce
            wavfile.write('q1_'+ sentence +'.wav', fs, data[int(i/100*fs):int((i+np.size(features_query_1, 1))/100*fs)])

    for i in range(np.size(features_sentence, 1) - np.size(features_query_2, 1) + 1):
        for j in range(np.size(features_query_2, 1)):
            corelation, tmp = pearsonr(features_query_2_transpose[j], features_sentence_transpose[i+j])
            d2[i] = d2[i] + corelation

    for i in range(np.size(features_sentence, 1) - np.size(features_query_2, 1) + 1):
        d2[i] = d2[i] / np.size(features_query_2, 1)
        
    # Vypsání jestli jsme query ve větě našly a extrakce do souboru
    # Výpočet vzorku: číslo rámce / počet rámců za 1s * vzorkovací frekvence
     # přeskočení pár následujících hodnot kde bude výsledek podobný
    for i in range(np.size(features_sentence, 1) - np.size(features_query_2, 1) + 1):
        if d2[i] >= q2_threshold:
            print('q2 se nachází ve větě '+ sentence +' od vzorku: '+ str(int(i/100*fs)) +' do vzorku: '+ str(int((i+np.size(features_query_2, 1))/100*fs)))
            # extrakce
            wavfile.write('q2_'+ sentence +'.wav', fs, data[int(i/100*fs):int((i+np.size(features_query_2, 1))/100*fs)])

    image[2].plot(np.arange(np.size(features_sentence, 1) - np.size(features_query_1, 1) + 1)/100 ,d1)
    image[2].set_xlim(xmin=0)
    image[2].set_xlim(xmax=np.size(features_sentence, 1)/100)
    image[2].set_ylabel('Skóre')
    image[2].set_xlabel('Čas [s]')

    image[2].plot(np.arange(np.size(features_sentence, 1) - np.size(features_query_2, 1) + 1)/100 ,d2)
    image[2].legend(['q1 = recognition', 'q2 = surrounded'])
    image[2].set_xlim(xmin=0)
    image[2].set_xlim(xmax=np.size(features_sentence, 1)/100)
    image[2].set_ylabel('Skóre')
    image[2].set_xlabel('Čas [s]')
    plt.tight_layout()
    #plt.savefig('result_'+ sentence +'.png')
    plt.show()

